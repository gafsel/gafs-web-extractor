/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor.util;

import br.gafs.web.extractor.util.ExpressionHandler.Token;

/**
 *
 * @author Gabriel
 */
public final class ExpressionHandler<T extends Token> {

    private String expression;

    private String textBefore = "";
    private String textAfter = "";
    private T token = null;
    private T nextToken = null;
    private int current = 0;
    private int next = -1;
    
    private T[] tokens;
    
    public static <T extends Token> ExpressionHandler create(String expression, T... tokens){
        return new ExpressionHandler(expression, tokens);
    }
    
    private ExpressionHandler(String expression, T... tokens) {
        this.expression = expression;
        this.tokens = tokens;
        
        defineNext();
    }

    public T getCurrent() {
        return token;
    }

    public String getTextAfter() {
        return textAfter;
    }

    public String getTextBefore() {
        return textBefore;
    }
    
    public boolean next(){
        if (next < 0){
            current = next;
            return false;
        }
        
        current = next + nextToken.getValor().length();
        token = nextToken;
        textBefore = textAfter;
        
        defineNext();
        
        return true;
    }

    private void defineNext() {
        int index = -1;
        for (T tkn : tokens){
            int idx = expression.indexOf(tkn.getValor(), next + 1);
            if (idx >= 0 && (index < 0 || idx < index)){
                index = idx;
                nextToken = tkn;
            }
        }
        next = index;
        textAfter = expression.substring(current, 
                next < 0 ? expression.length() : next).trim();
    }

    public boolean finished() {
        return current < 0;
    }
    
    public interface Token {
        public String getValor();
    }

    @Override
    public String toString() {
        return "ExpressionHandlerUtil{" + "expression=" + expression + ", textAfter=" + textAfter + ", token=" + token + ", nextToken=" + nextToken + ", current=" + current + ", next=" + next + '}';
    }
    
}
