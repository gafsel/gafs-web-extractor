/*
 * Criado por Gabriel Silva gafsel@gmail.com
 */

package br.gafs.web.extractor;

/**
 *
 * @author Gabriel
 */
@HtmlObject(filter = "::table[class eq sortable]::tr")
public class CardSummary {
    @HtmlProperty(filter = "::td{(::table[class eq sortable]::th{contains Card No})}")
    private String numero;
    @HtmlProperty(filter = "::td{(::table[class eq sortable]::th{contains Name})}")
    private String nome;
    @HtmlProperty(filter = "::td{(::table[class eq sortable]::th{contains Rarity})}")
    private String rarity;
    @HtmlAttributeProperty(filter = "::td{(::table[class eq sortable]::th{contains Name})}::a", attribute = "href")
    private String url;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "\n\tCardSummary{" + "nome=" + nome + ", url=" + url + '}';
    }
    
    
}
