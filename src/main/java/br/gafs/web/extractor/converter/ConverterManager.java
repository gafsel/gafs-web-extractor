/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor.converter;

import br.gafs.web.extractor.HtmlObject;
import br.gafs.web.extractor.exception.InvalidTypeException;
import br.gafs.web.extractor.navigator.ObjectNavigator;
import java.util.HashMap;
import java.util.Map;
import org.htmlcleaner.TagNode;

/**
 *
 * @author Gabriel
 */
public final class ConverterManager {

    private static Converter HTML_OBJECT_CONVERTER = new HtmlObjectConverter();
    private static Map<Class<?>, Converter> padrao = new HashMap<Class<?>, Converter>();
    
    static {
        for (ConversoresPadroes conversor : ConversoresPadroes.values()){
            padrao.put(conversor.getType(), conversor);
        }
    }
    
    public static Converter create(Class<?> type, Class<? extends Converter> conversor) {
        if (conversor.equals(Converter.class)){
            if (padrao.containsKey(type)){
                return padrao.get(type);
            }
            
            if (type.isAnnotationPresent(HtmlObject.class)){
                return HTML_OBJECT_CONVERTER;
            }
            
            return ConversoresPadroes.STRING;
        }
        try {
            return conversor.newInstance();
        } catch (Exception ex) {
            throw new InvalidTypeException(ex);
        }
    }
    
    enum ConversoresPadroes implements Converter {
        STRING(String.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.trim();
            }
            
        },
        INTEGER(Integer.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? null : Integer.parseInt(value);
            }
            
        },
        CHARACTER(Character.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? null : value.charAt(0);
            }
            
        },
        DOUBLE(Double.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? null : Double.parseDouble(value);
            }
            
        },
        FLOAT(Float.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? null : Float.parseFloat(value);
            }
            
        },
        LONG(Long.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? null : Long.parseLong(value);
            }
            
        },
        BOOLEAN(Boolean.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return Boolean.valueOf(value);
            }
            
        },
        PRIMITIVO_INT(int.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? 0 : Integer.parseInt(value);
            }
            
        },
        PRIMITIVO_FLOAT(float.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? 0 : Float.parseFloat(value);
            }
            
        },
        PRIMITIVO_LONG(long.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? 0 : Long.parseLong(value);
            }
            
        },
        PRIMITIVO_BOOLEAN(boolean.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return Boolean.parseBoolean(value);
            }
            
        },
        PRIMITIVO_DOUBLE(double.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return value.isEmpty() ? 0 : Double.parseDouble(value);
            }
            
        },
        PRIMITIVO_CHAR(char.class){

            public Object convert(Class<?> type, TagNode node, String value) {
                return (char) (value.isEmpty() ? 0 : value.charAt(0));
            }
            
        };
        
        private final Class<?> type;

        private ConversoresPadroes(Class<?> type) {
            this.type = type;
        }

        public Class<?> getType() {
            return type;
        }
        
    }
    
    static class HtmlObjectConverter implements Converter {

        public Object convert(Class<?> type, TagNode node, String value) {
            return ObjectNavigator.create(type).create(node);
        }
        
    }
}
