/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor.navigator;

import br.gafs.web.extractor.HtmlAttributeProperty;
import br.gafs.web.extractor.HtmlAttributePropertyCollection;
import br.gafs.web.extractor.HtmlProperty;
import br.gafs.web.extractor.HtmlPropertyCollection;
import br.gafs.web.extractor.converter.Converter;
import br.gafs.web.extractor.converter.ConverterManager;
import br.gafs.web.extractor.exception.InvalidTypeException;
import br.gafs.web.extractor.navigator.tag.TagNavigator;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.htmlcleaner.TagNode;

/**
 *
 * @author Gabriel
 */
public abstract class FieldNavigator {
    private TagNavigator tagNavigator;
    protected Converter conversor;
    protected Field field;

    FieldNavigator(Field field, Class<? extends Converter> conversor, String filter) {
        this.tagNavigator = TagNavigator.create(filter);
        this.conversor = ConverterManager.create(field.getType(), conversor);
        this.field = field;
    }
    
    static FieldNavigator create(Field field) {
        if (field.isAnnotationPresent(HtmlAttributeProperty.class)){
            return new AttributeFieldNavigator(field);
        }else if (field.isAnnotationPresent(HtmlProperty.class)){
            return new PropertyFieldNavigator(field);
        }else if (field.isAnnotationPresent(HtmlPropertyCollection.class)){
            return new PropertyCollectionFieldNavigator(field);
        }else if (field.isAnnotationPresent(HtmlAttributePropertyCollection.class)){
            return new AttributeCollectionFieldNavigator(field);
        }
        return null;
    }
    
    void handle(TagNode tagNode, Object obj){
        TagNode node = tagNavigator.findSingle(tagNode);
        if (node != null){
            found(node, obj);
        }
    }

    protected abstract void found(TagNode node, Object obj);
}

class PropertyFieldNavigator extends FieldNavigator {

    public PropertyFieldNavigator(Field field) {
        super(field, field.getAnnotation(HtmlProperty.class).converter(),
                field.getAnnotation(HtmlProperty.class).filter());
    }

    @Override
    protected void found(TagNode node, Object obj) {
        field.setAccessible(true);
        try {
            field.set(obj, conversor.convert(field.getType(), node, node.getText().toString()));
        } catch (Exception ex) {
            throw new InvalidTypeException(ex);
        }
        field.setAccessible(false);
    }
    
}

class AttributeFieldNavigator extends FieldNavigator {
    private String attribute;

    public AttributeFieldNavigator(Field field) {
        super(field, field.getAnnotation(HtmlAttributeProperty.class).converter(), 
                field.getAnnotation(HtmlAttributeProperty.class).filter());
        this.attribute = field.getAnnotation(HtmlAttributeProperty.class).attribute();
    }  
    
    @Override
    protected void found(TagNode node, Object obj) {
        field.setAccessible(true);
        try {
            field.set(obj, conversor.convert(field.getType(), node, node.getAttributeByName(attribute)));
        } catch (Exception ex) {
            throw new InvalidTypeException(ex);
        }
        field.setAccessible(false);
    }
}

class PropertyCollectionFieldNavigator extends FieldNavigator {

    private HtmlPropertyCollection a;
    private TagNavigator eachTagNavigator;
    private Converter objConverter;
    
    public PropertyCollectionFieldNavigator(Field field) {
        super(field, field.getAnnotation(HtmlPropertyCollection.class).property().converter(),
                field.getAnnotation(HtmlPropertyCollection.class).filter());
        this.a = field.getAnnotation(HtmlPropertyCollection.class);
        this.eachTagNavigator = TagNavigator.create(a.property().filter());
        this.objConverter = ConverterManager.create(a.forClass(), Converter.class);
    }
    
    @Override
    protected void found(TagNode node, Object obj) {
        List<TagNode> encontrados = eachTagNavigator.find(node);
        List resultado = new ArrayList();
        
        for (TagNode child : encontrados){
            Object o = objConverter.convert(a.forClass(), child, child.getText().toString());
            if (o != null){
                resultado.add(o);
            }
        }
                
        field.setAccessible(true);
        try {
            field.set(obj, resultado);
        } catch (Exception ex) {
            throw new InvalidTypeException(ex);
        }
        field.setAccessible(false);
    }
    
}

class AttributeCollectionFieldNavigator extends FieldNavigator {
    private String tag;
    private HtmlPropertyCollection a;
    private TagNavigator eachTagNavigator;
    private Converter objConverter;

    public AttributeCollectionFieldNavigator(Field field) {
        super(field, field.getAnnotation(HtmlAttributeProperty.class).converter(), 
                field.getAnnotation(HtmlAttributeProperty.class).filter());
        this.tag = field.getAnnotation(HtmlAttributeProperty.class).attribute();
        this.a = field.getAnnotation(HtmlPropertyCollection.class);
        this.eachTagNavigator = TagNavigator.create(a.property().filter());
        this.objConverter = ConverterManager.create(a.forClass(), Converter.class);
    }  
    
    @Override
    protected void found(TagNode node, Object obj) {
        List<TagNode> encontrados = eachTagNavigator.find(node);
        List resultado = new ArrayList();
        
        for (TagNode child : encontrados){
            Object o = objConverter.convert(a.forClass(), child, child.getAttributeByName(tag));
            if (o != null){
                resultado.add(o);
            }
        }
                
        field.setAccessible(true);
        try {
            field.set(obj, resultado);
        } catch (Exception ex) {
            throw new InvalidTypeException(ex);
        }
        field.setAccessible(false);
    }
}