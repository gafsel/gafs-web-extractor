package br.gafs.web.extractor.util;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class HtmlCleanerUtil {
    
    private static HtmlCleaner cleaner = new HtmlCleaner();
    
    static{
        cleaner.getProperties().setTranslateSpecialEntities(true);
        cleaner.getProperties().setOmitUnknownTags(false);
    }
    
    private static interface Cleaner<T>{
        public TagNode clean(T t) throws Exception;
    }
    
    public static TagNode cleanUrl(String url) throws MalformedURLException, IOException{
        return cleanUrl(new URL(url));
    }
    
    public static TagNode cleanUrl(InputStream is) throws IOException {
        return clean(is, new Cleaner<InputStream>(){
            
            @Override
            public TagNode clean(InputStream t) throws Exception {
                return cleaner.clean(t);
            }
            
        });
    }
    
    public static TagNode cleanUrl(URL url) throws IOException {
        return clean(url, new Cleaner<URL>(){
            
            @Override
            public TagNode clean(URL t) throws Exception {
                return cleaner.clean(t);
            }
            
        });
    }

    public static TagNode cleanUrl(String url, final String charset) throws MalformedURLException, IOException{
        return cleanUrl(new URL(url), charset);
    }

    public static TagNode cleanUrl(InputStream is, final String charset) throws IOException {
        return clean(is, new Cleaner<InputStream>(){

            @Override
            public TagNode clean(InputStream t) throws Exception {
                return cleaner.clean(t, charset);
            }

        });
    }

    public static TagNode cleanUrl(URL url, final String charset) throws IOException {
        return clean(url, new Cleaner<URL>(){

            @Override
            public TagNode clean(URL t) throws Exception {
                return cleaner.clean(t.openStream(), charset);
            }

        });
    }

    public static TagNode cleanHtml(String html) throws MalformedURLException, IOException{
        return clean(html, new Cleaner<String>(){
            
            @Override
            public TagNode clean(String t) throws Exception {
                return cleaner.clean(t);
            }
            
        });
    }
    
    private static <T> TagNode clean(T t, Cleaner<T> cleaner) throws MalformedURLException, IOException{
        try{
            return cleaner.clean(t);
        }catch(Exception e){
            System.err.println("Problema com a leitura "+t);
            e.printStackTrace();
            return new TagNode("empty");
        }
    }
}
