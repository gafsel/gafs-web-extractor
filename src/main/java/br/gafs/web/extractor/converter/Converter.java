/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor.converter;

import org.htmlcleaner.TagNode;

/**
 *
 * @author Gabriel
 */
public interface Converter {
    Object convert(Class<?> type, TagNode node, String value);
}
