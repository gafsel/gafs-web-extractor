/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package br.gafs.web.extractor.navigator.tag;

import br.gafs.web.extractor.util.ExpressionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.htmlcleaner.TagNode;

/**
 *
 * @author Gabriel
 */
public class DirectTagNavigator extends TagNavigator {

    protected DirectTagNavigator(ExpressionHandler<TagNavigatorToken> handler) {
        super(handler);
    }
    
    public List<TagNode> find(TagNode tagNode) {
        List<TagNode> nodes = new ArrayList<TagNode>();
        
        if (is(tagNode)){
            if (hasNext()){
                for (TagNode child : tagNode.getChildTags()){
                    nodes.addAll(next.find(child));
                }
            }else{
                nodes.add(tagNode);
            }
        }
        
        return nodes;
    }
    
    public boolean is(TagNode tagNode) {
        if (!tagNode.getName().equalsIgnoreCase(name)){
            return false;
        }
        
        for (Map.Entry<String, Matcher> entry : atributos.entrySet()){
            if (!entry.getValue().matches(tagNode.
                    getAttributes().get(entry.getKey()))){
                return false;
            }
        }
        
        for (TagNavigator navigator : children){
            if (!navigator.isIn(tagNode)){
                return false;
            }
        }
        
        return true;
    }
    
    public boolean isIn(TagNode tagNode) {
        for (TagNode child : tagNode.getChildTags()){
            if (is(child)){
                return true;
            }
        }
        
        return false;
    }
    
    public TagNode findSingle(TagNode tagNode) {
        if (is(tagNode)){
            if (hasNext()){
                for (TagNode child : tagNode.getChildTags()){
                    TagNode found = next.findSingle(child);
                    if (found != null){
                        return found;
                    }
                }
            }else{
                return tagNode;
            }
        }
        
        return null;
    }
}
