/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor;

import br.gafs.web.extractor.navigator.ObjectNavigator;
import br.gafs.web.extractor.util.HtmlCleanerUtil;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.htmlcleaner.TagNode;

/**
 *
 * @author Gabriel
 */
public class Extractor {


    public static final String DEFAULT_CHARSET = "UTF-8";

    public static <T> List<T> extract(InputStream is, Class<T>... types) throws IOException {
        return extract(is, DEFAULT_CHARSET, types);
    }

    public static <T> List<T> extract(InputStream is, String charset, Class<T>... types) throws IOException {
        TagNode root = HtmlCleanerUtil.cleanUrl(is, charset);
        List<T> ts = new ArrayList<T>();
        for (Class<T> t : types){
            ts.addAll((List) ObjectNavigator.create(t).find(root));
        }
        return ts;
    }

    public static <T> List<T> extract(URL url, Class<T>... types) throws IOException {
        return extract(url, DEFAULT_CHARSET, types);
    }

    public static <T> List<T> extract(URL url, String charset, Class<T>... types) throws IOException {
        TagNode root = HtmlCleanerUtil.cleanUrl(url, charset);
        List<T> ts = new ArrayList<T>();
        for (Class<T> t : types){
            ts.addAll((List) ObjectNavigator.create(t).find(root));
        }
        return ts;
    }

    public static <T> T extractSingle(InputStream is, Class<T> type) throws IOException {
        return extractSingle(is, DEFAULT_CHARSET, type);
    }

    public static <T> T extractSingle(InputStream is, String charset, Class<T> type) throws IOException {
        TagNode root = HtmlCleanerUtil.cleanUrl(is, charset);
        return (T) ObjectNavigator.create(type).findSingle(root);
    }

    public static <T> T extractSingle(URL url, Class<T> type) throws IOException {
        return extractSingle(url, DEFAULT_CHARSET, type);
    }

    public static <T> T extractSingle(URL url, String charset, Class<T> type) throws IOException {
        TagNode root = HtmlCleanerUtil.cleanUrl(url);
        return (T) ObjectNavigator.create(type).findSingle(root);
    }

}
