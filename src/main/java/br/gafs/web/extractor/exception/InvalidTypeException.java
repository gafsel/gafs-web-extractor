/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package br.gafs.web.extractor.exception;

/**
 *
 * @author Gabriel
 */
public class InvalidTypeException extends ExtractorException {
    
    public InvalidTypeException() {
    }
    
    public InvalidTypeException(Exception ex) {
        super(ex);
    }
    
}
