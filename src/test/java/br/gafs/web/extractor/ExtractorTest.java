/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author Gabriel
 */
public class ExtractorTest {
    public static void main(String... args) throws MalformedURLException, IOException{
        List<CardSummary> cards = Extractor.
                extract(new URL("http://cardfight.wikia.com/wiki/G_Booster_Set_3:_Sovereign_Star_Dragon"), CardSummary.class);
        
        for (CardSummary card : cards){
            System.out.println(">> "+card.getNome());
            System.out.println(">> "+card.getNumero());
            System.out.println(">> "+card.getRarity());
            System.out.println(">> "+card.getUrl());
            System.out.println("================================================");
        }
    }
}
