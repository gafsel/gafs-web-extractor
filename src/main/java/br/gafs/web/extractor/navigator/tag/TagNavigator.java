/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package br.gafs.web.extractor.navigator.tag;

import br.gafs.web.extractor.exception.InvalidFilterException;
import br.gafs.web.extractor.util.ExpressionHandler;
import br.gafs.web.extractor.util.ExpressionHandler.Token;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.htmlcleaner.TagNode;

/**
 *
 * @author Gabriel
 */
public class TagNavigator {
    
    protected String name;
    protected Map<String, Matcher> atributos = new HashMap<String, Matcher>();
    protected List<TagNavigator> children = new ArrayList<TagNavigator>();
    protected Matcher childrenText;
    protected Integer tagIndex;
    protected TagNavigator indexNavigator;
    protected TagNavigator next;
    
    public static TagNavigator create(String filter) {
        return create(ExpressionHandler.create(filter, TagNavigatorToken.values()));
    }
    
    private static TagNavigator create(ExpressionHandler<TagNavigatorToken> handler){
        if (handler.next()){
            switch (handler.getCurrent()){
                case TAG:
                    return new TagNavigator(handler);
                case DIRECT_TAG:
                    return new DirectTagNavigator(handler);
            }
        }

        throw new InvalidFilterException("Começo de tag errado: "+handler);
    }
    
    protected TagNavigator(ExpressionHandler<TagNavigatorToken> handler) {
        prepare(handler);
    }
    
    private void prepare(ExpressionHandler<TagNavigatorToken> handler){
        this.name = handler.getTextAfter();

        if (handler.next()){
            do{
                switch (handler.getCurrent()){
                    case TAG:
                        next = new TagNavigator(handler);
                        break;
                    case DIRECT_TAG:
                        next = new DirectTagNavigator(handler);
                        break;
                    case ATTR_OPEN:
                        preparaAttributes(handler);
                        break;
                    case CHILD_OPEN:
                        preparaChildren(handler);
                        break;
                    default:
                        return;
                }
            }while (!handler.finished());
        }
        
    }
    
    public boolean hasNext(){
        return next != null;
    }
    
    private void preparaAttributes(ExpressionHandler<TagNavigatorToken> handler) {
        String value = handler.getTextAfter().trim();
        
        if (value.matches("[0-9]+")){
            tagIndex = Integer.parseInt(value);
            if (!handler.next()){
                throw new InvalidFilterException();
            }
        }else if (handler.next() && TagNavigatorToken.IDX_OPEN.equals(handler.getCurrent())){
            indexNavigator = create(handler);
            if (!TagNavigatorToken.IDX_CLOSE.equals(handler.getCurrent()) || !handler.next()){
                throw new InvalidFilterException();
            }
        }else{
            String attributes[] = value.split("\\s*,\\s*");
            for (String attribute : attributes){
                ExpressionHandler<AttributeVerifierToken> expHandler = 
                        ExpressionHandler.create(attribute, 
                        AttributeVerifierToken.values());

                if (!expHandler.next()){
                    throw new InvalidFilterException("Expressão atributo inválida:\n\t "+expHandler);
                }

                this.atributos.put(expHandler.getTextBefore(), 
                        new Matcher(expHandler.getCurrent(), 
                                    expHandler.getTextAfter()));
            }
        }
        
        if (!TagNavigatorToken.ATTR_CLOSE.equals(handler.getCurrent())){
            throw new InvalidFilterException();
        }

        handler.next();
    }
    
    private void preparaChildren(ExpressionHandler<TagNavigatorToken> handler) {
        if (handler.next()){
            do{
                switch (handler.getCurrent()){
                    case CHILD_CLOSE:
                        if (children.isEmpty()){
                            ExpressionHandler<ChildrenTextVerifierToken> expHandler = 
                                    ExpressionHandler.create(handler.getTextBefore(), 
                                            ChildrenTextVerifierToken.values());
                            if (expHandler.next()){
                                childrenText = new Matcher(expHandler.getCurrent(), 
                                        expHandler.getTextAfter());
                            }else if (!handler.getTextBefore().isEmpty()){
                                childrenText = new Matcher(ChildrenTextVerifierToken.EQ, 
                                        handler.getTextBefore());
                            }
                        }
                        handler.next();
                        return;
                    case TAG:
                        children.add(new TagNavigator(handler));
                        break;
                    case DIRECT_TAG:
                        children.add(new DirectTagNavigator(handler));
                        break;
                    case CHILD_SPLIT:
                        handler.next();
                        break;
                    default:
                        throw new InvalidFilterException("Tipo de token inválido encontrado em filter children:\n\t "+handler);
                }
            }while(!handler.finished());
        }
        
        throw new InvalidFilterException("Final preciptado em filter children:\n\t "+handler);
    }
    
    public Map<String, Matcher> getAtributos() {
        return atributos;
    }
    
    public List<TagNavigator> getChildren() {
        return children;
    }
    
    public String getName() {
        return name;
    }
    
    public TagNavigator getNext() {
        return next;
    }
    
    public List<TagNode> find(TagNode tagNode) {
        List<TagNode> nodes = new ArrayList<TagNode>();
        
        if (is(tagNode)){
            if (hasNext()){
                for (TagNode child : tagNode.getChildTags()){
                    nodes.addAll(next.find(child));
                }
            }else{
                nodes.add(tagNode);
            }
        }else{
            for (TagNode child : tagNode.getChildTags()){
                nodes.addAll(find(child));
            }
        }
        
        return nodes;
    }
    
    public boolean is(TagNode tagNode) {
        if (!tagNode.getName().equalsIgnoreCase(name)){
            return false;
        }
        
        if (tagIndex != null){
            if (!tagIndex.equals(tagNode.
                    getParent().getChildTagList().indexOf(tagNode))){
                return false;
            }
        }else if (indexNavigator != null) {
            if (!getIndex(tagNode).equals(getIndex(indexNavigator.findSingle(getRoot(tagNode))))){
                return false;
            }
        }else{
            for (Entry<String, Matcher> entry : atributos.entrySet()){
                if (!entry.getValue().matches(tagNode.
                        getAttributes().get(entry.getKey()))){
                    return false;
                }
            }
        }
        
        for (TagNavigator navigator : children){
            if (!navigator.isIn(tagNode)){
                return false;
            }
        }
        
        if (childrenText != null){
            if (!childrenText.matches(tagNode.getText().toString())){
                return false;
            }
        }
        
        return true;
    }
    
    public boolean isIn(TagNode tagNode) {
        for (TagNode child : tagNode.getChildTags()){
            if (is(child)){
                return true;
            }
        }
        
        for (TagNode child : tagNode.getChildTags()){
            if (isIn(child)){
                return true;
            }
        }
        
        return false;
    }

    public TagNode findSingle(TagNode tagNode) {
        if (is(tagNode)){
            if (hasNext()){
                for (TagNode child : tagNode.getChildTags()){
                    TagNode found = next.findSingle(child);
                    if (found != null){
                        return found;
                    }
                }
            }else{
                return tagNode;
            }
        }else{
            for (TagNode child : tagNode.getChildTags()){
                TagNode found = findSingle(child);
                if (found != null){
                    return found;
                }
            }
        }
        
        return null;
    }

    private Integer getIndex(TagNode tagNode) {
        if (tagNode == null){
            return -1;
        }
        
        return tagNode.getParent().getChildTagList().indexOf(tagNode);
    }

    private TagNode getRoot(TagNode tagNode) {
        if (tagNode.getParent() == null){
            return tagNode;
        }
        
        return getRoot(tagNode.getParent());
    }

    enum TagNavigatorToken implements Token {
        TAG("::"),
        DIRECT_TAG(">:"),
        ATTR_OPEN("["),
        ATTR_CLOSE("]"),
        IDX_OPEN("("),
        IDX_CLOSE(")"),
        CHILD_OPEN("{"),
        CHILD_SPLIT("|"),
        CHILD_CLOSE("}");
        
        private final String valor;
        
        private TagNavigatorToken(String valor) {
            this.valor = valor;
        }
        
        public String getValor() {
            return valor;
        }
    }
    
    interface AttributeVerifier {
        boolean matches(String pattern, String value);
    }
    
    class Matcher {
        private AttributeVerifier verifier;
        private String pattern;

        public Matcher(AttributeVerifier verifier, String pattern) {
            this.verifier = verifier;
            this.pattern = pattern;
        }
        
        public boolean matches(String value){
            return value != null && verifier.matches(pattern, value.trim());
        }

        public String getPattern() {
            return pattern;
        }
    }
    
    enum ChildrenTextVerifierToken implements Token, AttributeVerifier {
        EQ("eq "){

            public boolean matches(String pattern, String value) {
                return pattern.equals(value);
            }
            
        },
        CONTAINS("contains "){

            public boolean matches(String pattern, String value) {
                return value.contains(pattern);
            }
            
        },
        NOT("not "){

            public boolean matches(String pattern, String value) {
                return !value.equals(pattern);
            }
            
        },
        MATCHES("matches "){

            public boolean matches(String pattern, String value) {
                return value.matches(pattern);
            }
            
        };
        
        private final String valor;
        
        private ChildrenTextVerifierToken(String valor) {
            this.valor = valor;
        }
        
        public String getValor() {
            return valor;
        }
    }
    
    enum AttributeVerifierToken implements Token, AttributeVerifier {
        EQ(" eq "){

            public boolean matches(String pattern, String value) {
                return pattern.equals(value);
            }
            
        },
        CONTAINS(" contains "){

            public boolean matches(String pattern, String value) {
                return value.contains(pattern);
            }
            
        },
        NOT(" not "){

            public boolean matches(String pattern, String value) {
                return !value.equals(pattern);
            }
            
        },
        MATCHES(" matches "){

            public boolean matches(String pattern, String value) {
                return value.matches(pattern);
            }
            
        };
        
        private final String valor;
        
        private AttributeVerifierToken(String valor) {
            this.valor = valor;
        }
        
        public String getValor() {
            return valor;
        }
    }
    
    
}
