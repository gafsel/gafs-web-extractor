/*
 * Criado por Gabriel Silva gafsel@gmail.com
 */

package br.gafs.web.extractor;

import java.util.List;

/**
 *
 * @author Gabriel
 */
@HtmlObject(filter = "::div[id eq mw-content-text]")
public class CardDetail {
    @HtmlAttributeProperty(filter = "::a[class eq image image-thumbnail]::img", attribute = "src")
    private String imagem;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Name }}::td{ not Name }")
    private String nome;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Unit Type }}::td{ not Unit Type }")
    private String unitType;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Grade / Skill }}::td{ not Grade / Skill }")
    private String gradeSkill;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Power }}::td{ not Power }")
    private String power;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Critical }}::td{ not Critical }")
    private String critical;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Shield }}::td{ not Shield }")
    private String shield;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Nation }}::td{ not Nation }")
    private String nation;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Clan }}::td{ not Clan }")
    private String clan;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Race }}::td{ not Race }")
    private String race;
    @HtmlProperty(filter = "::div[class contains info-main]::table::tr{ ::td{ Trigger }}::td{ not Trigger }")
    private String trigger;
    @HtmlPropertyCollection(filter = "::div[class contains info-extra sets]::table::tr::td::ul",
            forClass = String.class, property = @HtmlProperty(filter = "::li::a"))
    private List<String> sets;
    @HtmlProperty(filter = "::div[class contains info-extra flavor]::table::tr::td")
    private String flavors;
    @HtmlProperty(filter = "::div[class contains info-extra effect]::table::tr::td")
    private String texto;

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getGradeSkill() {
        return gradeSkill;
    }

    public void setGradeSkill(String gradeSkill) {
        this.gradeSkill = gradeSkill;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getCritical() {
        return critical;
    }

    public void setCritical(String critical) {
        this.critical = critical;
    }

    public String getShield() {
        return shield;
    }

    public void setShield(String shield) {
        this.shield = shield;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public List<String> getSets() {
        return sets;
    }

    public void setSets(List<String> sets) {
        this.sets = sets;
    }

    public String getFlavors() {
        return flavors;
    }

    public void setFlavors(String flavors) {
        this.flavors = flavors;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public String toString() {
        return "CardDetail{" + "imagem=" + imagem + ",\n\tnome=" + nome + ",\n\tunitType=" + unitType + ",\n\tgradeSkill=" + gradeSkill + ",\n\tpower=" + power + ",\n\tcritical=" + critical + ",\n\tshield=" + shield + ",\n\tnation=" + nation + ",\n\tclan=" + clan + ",\n\trace=" + race + ",\n\ttrigger=" + trigger + ",\n\tsets=" + sets + ",\n\tflavors=" + flavors + ",\n\ttexto=" + texto + '}';
    }
    
    
}
