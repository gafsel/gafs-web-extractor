/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor.navigator;

import br.gafs.web.extractor.HtmlObject;
import br.gafs.web.extractor.exception.InvalidTypeException;
import br.gafs.web.extractor.navigator.tag.TagNavigator;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.htmlcleaner.TagNode;

/**
 *
 * @author Gabriel
 */
public class ObjectNavigator {
    private static Map<Class<?>, ObjectNavigator> cache = 
            new HashMap<Class<?>, ObjectNavigator>();
    
    private TagNavigator tagNavigator;
    private Class<?> type;
    private List<FieldNavigator> fields = new ArrayList<FieldNavigator>();

    public static ObjectNavigator create(Class<?> type){
        if (!cache.containsKey(type)){
            cache.put(type,  new ObjectNavigator(type));
        }
        
        return cache.get(type);
    }
    
    private ObjectNavigator(Class<?> type) {
        if (!type.isAnnotationPresent(HtmlObject.class)){
            throw new InvalidTypeException();
        }
        
        this.tagNavigator = TagNavigator.create(type.
                getAnnotation(HtmlObject.class).filter());
        this.type = type;

        extractFields(type);
    }

    private void extractFields(Class<?> type) {
        for (Field field : type.getDeclaredFields()){
            FieldNavigator nav = FieldNavigator.create(field);
            if (nav != null){
                fields.add(nav);
            }
        }
        
        if (!type.getSuperclass().equals(Object.class)){
            extractFields(type.getSuperclass());
        }
    }

    public List<Object> find(TagNode node){
        List<TagNode> found = tagNavigator.find(node);
        
        List<Object> objs = new ArrayList<Object>();
        
        for (TagNode tag : found){
            objs.add(create(tag));
        }
        
        return objs;
    }
    
    public Object create(TagNode found){
        Object o;
        try {
            o = type.newInstance();
        } catch (Exception ex) {
            throw new InvalidTypeException(ex);
        }
        
        for (FieldNavigator field : fields){
            field.handle(found, o);
        }
        
        return o;
    }
    
    public Object findSingle(TagNode node){
        TagNode found = tagNavigator.findSingle(node);
        
        if (found != null){
            return create(found);
        }
        
        return null;
    }
    
}
