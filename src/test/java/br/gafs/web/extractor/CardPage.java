/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gafs.web.extractor;

import java.util.List;

/**
 *
 * @author Gabriel
 */
@HtmlObject(filter = "::div[id eq mw-pages]")
public class CardPage {
    @HtmlAttributeProperty(filter = "::a{ contains next }", attribute = "href")
    private String next;
    @HtmlPropertyCollection(
            filter = "::table",
            forClass = CardSummary.class,
            property = @HtmlProperty(filter = "::td::ul::li"))
    private List<CardSummary> cards;

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<CardSummary> getCards() {
        return cards;
    }

    public void setCards(List<CardSummary> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "CardPage{" + "next=" + next + ", cards=" + cards + '}';
    }
    
    
}
